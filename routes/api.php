<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('token', 'AuthController@token');
});

Route::group(['prefix' => 'tokens'], function () {
    Route::post('verify', 'TokensController@verify');
});

Route::group(['prefix' => 'users'], function () {
    Route::put('', 'UsersController@update');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('logout', 'AuthController@logout');
    Route::get('me', 'AuthController@me');

    Route::group(['prefix' => 'admin/users', 'middleware' => 'scopes:admin-access'], function () {
        Route::get('', 'Admin\UsersController@all');
        Route::post('', 'Admin\UsersController@add');
    });

    Route::group(['prefix' => 'verifications'], function () {
        Route::post('', 'VerificationsController@submit');
    });

    Route::group(['prefix' => 'admin/verifications', 'middleware' => 'scopes:admin-access'], function () {
        Route::post('', 'Admin\VerificationsController@all');
        Route::post('new', 'Admin\VerificationsController@request');
    });

    Route::group(['prefix' => 'admin/settings', 'middleware' => 'scopes:admin-access'], function () {
        Route::get('', 'Admin\SettingsController@all');
    });
});
