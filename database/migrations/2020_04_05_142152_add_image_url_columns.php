<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageUrlColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('verifications', function (Blueprint $table) {
            $table->string('image_url')->after('lng');
        });
        Schema::table('quarantines', function (Blueprint $table) {
            $table->string('image_url')->after('lng');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('verifications', function (Blueprint $table) {
            $table->dropColumn('image_url');
        });
        Schema::table('quarantines', function (Blueprint $table) {
            $table->dropColumn('image_url');
        });
    }
}
