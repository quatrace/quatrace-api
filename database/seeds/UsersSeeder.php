<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Dimitar Dishev',
            'email' => 'dishev@lab08.com',
            'password' => bcrypt('qwerty123')
        ]);

        User::create([
            'name' => 'Nikolay Manchev',
            'email' => 'nikolay.manchev@lab08.com',
            'password' => bcrypt('qwerty123')
        ]);

        User::create([
            'name' => 'Milen Dimitrov',
            'email' => 'milen.dimitrov@itido.eu',
            'password' => bcrypt('qwerty123')
        ]);

        User::create([
            'name' => 'Ivan Petrov',
            'email' => 'ivan.petrov@quatrace.com',
            'password' => bcrypt('qwerty123')
        ]);

        User::create([
            'name' => 'Petar Georgiev',
            'email' => 'petar.georgiev@quatrace.com',
            'password' => bcrypt('qwerty123')
        ]);

        User::create([
            'name' => 'Georgi Todorov',
            'email' => 'georgi.todorov@quatrace.com',
            'password' => bcrypt('qwerty123')
        ]);
    }
}
