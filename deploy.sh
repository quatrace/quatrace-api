#!/bin/bash

function exe {
	echo
	echo "-----------"
	echo "Execute: $@"
	$@
	if [[ $? != 0 ]]; then
		echo "ERROR"
		exit
	fi
}

who=`whoami`
echo "Please wait file getting diff..."
exe "git fetch"
branch=`git branch | awk '/^\*/{print $2}'`
echo "Branch detected: ${branch}"
echo "The following files are modified/will be changes by an update:"
exe "git --no-pager diff --name-status -R origin/${branch}"
pwd=`pwd`
echo "Hit enter to update ${pwd}?"
read
exe "php artisan down"

exe "git reset --hard"
exe "git pull"
exe "composer install --no-interaction --prefer-dist --optimize-autoloader --no-dev"
exe "php artisan migrate --force"

exe "php artisan cache:clear"
exe "php artisan config:clear"
exe "php artisan view:clear"
exe "php artisan config:cache"
exe "php artisan horizon:terminate"

exe "supervisorctl reload"
sleep 3
exe "supervisorctl restart  quatrace-worker:*"

exe "chgrp -R www-data storage bootstrap/cache"
exe "chmod -R ug+rwx storage bootstrap/cache"

exe "php artisan up"
