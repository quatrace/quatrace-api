<?php

namespace App\Types;

class FaceComparisonResult
{
    protected string $reason;

    protected bool $isIdentical;

    protected float $confidence;

    public function __construct()
    {
        $this->reason = '';
        $this->isIdentical = false;
        $this->confidence = 0.00;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return \App\Types\FaceComparisonResult
     */
    public function setReason(string $reason): FaceComparisonResult
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIdentical(): bool
    {
        return $this->isIdentical;
    }

    /**
     * @param bool $isIdentical
     * @return \App\Types\FaceComparisonResult
     */
    public function setIsIdentical(bool $isIdentical): FaceComparisonResult
    {
        $this->isIdentical = $isIdentical;
        return $this;
    }

    /**
     * @return float
     */
    public function getConfidence(): float
    {
        return $this->confidence;
    }

    /**
     * @param float $confidence
     * @return \App\Types\FaceComparisonResult
     */
    public function setConfidence(float $confidence): FaceComparisonResult
    {
        $this->confidence = $confidence;
        return $this;
    }

}
