<?php

namespace App\Constants;

abstract class VerificationStatus
{
    use Retrievable;

    const NOT_SEND = 'NOT_SEND';
    const PENDING = 'PENDING';
    const FAILED = 'FAILED';
    const REJECTED = 'REJECTED';
    const APPROVED = 'APPROVED';
}
