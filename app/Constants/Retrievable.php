<?php

namespace App\Constants;

trait Retrievable
{
    public static function getAll()
    {
        $oClass = new \ReflectionClass(self::class);
        return $oClass->getConstants();
    }

    public static function getKey(string $value)
    {
        $oClass = new \ReflectionClass(self::class);
        $all = $oClass->getConstants();
        return array_search($value, $all);
    }

    public static function getValue(string $key)
    {
        $oClass = new \ReflectionClass(self::class);
        $all = $oClass->getConstants();
        return isset($all[$key]) ? $all[$key] : null;
    }
}
