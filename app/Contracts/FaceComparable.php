<?php

namespace App\Contracts;

use App\Types\FaceComparisonResult;

interface FaceComparable
{

    /**
     * @param string $faceOne - base64 representation of the face
     * @param string $faceTwo - base64 representation of the face
     * @return FaceComparisonResult
     */
    public function compare(string $faceOne, string $faceTwo): FaceComparisonResult;
}
