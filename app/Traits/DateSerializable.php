<?php

namespace App\Traits;

trait DateSerializable
{
    protected function serializeDate(\DateTimeInterface $date)
    {
        return $date->getTimestamp();
    }
}
