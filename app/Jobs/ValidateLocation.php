<?php

namespace App\Jobs;

use App\Services\VerificationService;
use App\Verification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ValidateLocation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Verification $verification;

    /**
     * ValidateLocation constructor.
     * @param \App\Verification $verification
     */
    public function __construct(Verification $verification)
    {
        $this->verification = $verification;
    }

    /**
     * @param \App\Services\VerificationService $verificationService
     */
    public function handle(VerificationService $verificationService)
    {
        $verificationService->validate($this->verification);
    }
}
