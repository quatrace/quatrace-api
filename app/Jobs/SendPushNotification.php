<?php

namespace App\Jobs;

use App\Constants\VerificationStatus;
use App\Verification;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class SendPushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Verification $verification;

    /**
     * SendPushNotification constructor.
     * @param \App\Verification $verification
     */
    public function __construct(Verification $verification)
    {
        $this->verification = $verification;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->verification->user;
        if (!$user) {
            return;
        }
        if (!$this->verification->user->quarantine) {
            return;
        }
        $key = $this->verification->user->quarantine->fcm_key;
        if ($key) {
            $message = CloudMessage::withTarget('token', $key)
                ->withNotification(Notification::create(__('Verify your location'), __('You have to validate your location!')))
                ->withData([
                    'token' => $this->verification->token,
                    'click_action' => 'FLUTTER_NOTIFICATION_CLICK'
                ]);
            try {
                app('firebase.messaging')->send($message);
                $this->verification->pn_send_at = Carbon::now();
                $this->verification->save();
            } catch (\Exception $e) {
                $this->verification->reason = $e->getMessage();
                $this->verification->status = VerificationStatus::FAILED;
                $this->verification->save();
            }
        }
    }
}
