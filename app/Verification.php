<?php

namespace App;

use App\Constants\VerificationStatus;
use App\Traits\DateSerializable;
use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{
    use DateSerializable;

    protected $fillable = [
        'user_id',
        'lat',
        'lng',
        'token',
        'status',
        'reason',
        'points',
        'distance',
        'is_identical',
        'confidence',
        'pn_send_at',
        'expires_at',
    ];

    protected $dates = [
        'pn_send_at',
        'expires_at'
    ];

    protected $casts = [
        'is_identical' => 'bool',
        'lat' => 'float',
        'lng' => 'float',
        'distance' => 'float'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeShowOnMobile($query)
    {
        $query->whereIn('status', [
                VerificationStatus::PENDING,
                VerificationStatus::REJECTED,
                VerificationStatus::APPROVED]
        );
    }
}
