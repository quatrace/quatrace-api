<?php

namespace App;

use App\Traits\DateSerializable;
use Illuminate\Database\Eloquent\Model;

class Quarantine extends Model
{
    use DateSerializable;

    protected $fillable = [
        'user_id',
        'phone',
        'ucn',
        'fcm_key',
        'lat',
        'lng',
        'expires_at',
    ];

    protected $dates = [
        'expires_at'
    ];

    protected $casts = [
        'lat' => 'float',
        'lng' => 'float'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
