<?php

namespace App\Console\Commands;

use App\Constants\VerificationStatus;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class RequestVerifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verifications:request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $quarantinedUsers = User::whereHas('quarantine', function ($query) {
            $query->where('expires_at', '>=', Carbon::now())->whereNotNull('fcm_key');
        })->get();
        foreach ($quarantinedUsers as $user) {
            Log::debug('RequestVerifications', [$user]);
            if ($user->settings->verification_frequency_minutes === 0) {
                Log::debug('verification_frequency_minutes continue', [$user->settings]);
                continue;
            }
            $lastVerification = $user->verifications->first();
            if ($lastVerification) {
                $minsPassedAfterLastVerification = Carbon::now()->diffInMinutes($lastVerification->created_at);
                Log::debug('$minsPassedAfterLastVerification', [$minsPassedAfterLastVerification]);
                if ($minsPassedAfterLastVerification < $user->settings->verification_frequency_minutes) {
                    continue;
                }
            }
            $user->verifications()->create([
                'token' => Str::random(config('quatrace.verification_token_length')),
                'expires_at' => Carbon::now()->addMinutes($user->settings->verification_ttl_minutes),
                'status' => VerificationStatus::NOT_SEND
            ]);
        }
    }
}
