<?php

namespace App\Console\Commands;

use App\Constants\VerificationStatus;
use App\Jobs\SendPushNotification;
use App\User;
use App\Verification;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DispatchPushNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch:push-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notifications to quarantined to request their location';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $verifications = Verification::where('expires_at', '>=', Carbon::now())
            ->whereNull('pn_send_at')
            ->whereNotNull('token')
            ->where('status', VerificationStatus::NOT_SEND)
            ->with('user')
            ->get();
        foreach ($verifications as $verification) {
            SendPushNotification::dispatch($verification)->onQueue('notifications');
        }
    }
}
