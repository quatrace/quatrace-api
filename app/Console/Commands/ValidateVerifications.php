<?php

namespace App\Console\Commands;

use App\Constants\VerificationStatus;
use App\Jobs\ValidateLocation;
use App\Verification;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ValidateVerifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verifications:validate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Go through pending verifications and check them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $verifications = Verification::where('expires_at', '>=', Carbon::now())
            ->where('status', VerificationStatus::PENDING)
            ->whereNotNull('lat')
            ->whereNotNull('lng')
            ->with('user')
            ->get();
        foreach ($verifications as $verification) {
            ValidateLocation::dispatch($verification);
        }
    }
}
