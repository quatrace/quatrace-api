<?php

namespace App\Console\Commands;

use App\Constants\VerificationStatus;
use App\Verification;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RejectExpiredVerifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verifications:reject-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reject expired verifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $expiredVerifications = Verification::where('expires_at', '<', Carbon::now())
            ->whereIn('status', [VerificationStatus::NOT_SEND, VerificationStatus::PENDING])
            ->get();
        foreach ($expiredVerifications as $verification) {
            $verification->reason = 'Expired';
            $verification->status = VerificationStatus::REJECTED;
            $verification->save();
        }
    }
}
