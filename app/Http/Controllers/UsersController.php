<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Token;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UsersController extends Controller
{

    public function update(UpdateUserRequest $request)
    {
        $token = Token::where('payload', $request->token)->valid()->first();
        if (!$token) {
            return response()->json([
                'message' => 'Token has expired. Please contact support for new one'
            ], 401);
        }

        $user = User::where('id', $token->user_id)->first();
        if (!$user || !$user->quarantine) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $user->quarantine->fcm_key = $request->fcm_key;
        $fileName = Str::random(64) . '.jpg';
        Storage::put($fileName, base64_decode($request->image));
        $user->quarantine->image_url = $fileName;
        $user->quarantine->save();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ]);
    }

}
