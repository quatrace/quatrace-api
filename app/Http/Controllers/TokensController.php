<?php

namespace App\Http\Controllers;

use App\Http\Requests\VerifyTokenRequest;
use App\Token;

class TokensController extends Controller
{
    /**
     * @param \App\Http\Requests\VerifyTokenRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(VerifyTokenRequest $request)
    {
        return response()->json([
            'result' => Token::where('payload', $request->token)->valid()->exists()
        ]);
    }
}
