<?php

namespace App\Http\Controllers;

use App\Constants\VerificationStatus;
use App\Http\Requests\NewVerificationRequest;
use App\Verification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class VerificationsController extends Controller
{
    /**
     * Submit location for verification
     *
     * @param \App\Http\Requests\NewVerificationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submit(NewVerificationRequest $request)
    {
        $verification = Verification::where('token', $request->token)->first();
        if (!$verification) {
            return response()->json([
                'message' => 'Incorrect token, verification not found!'
            ], 404);
        }
        $verification->status = VerificationStatus::PENDING;
        $verification->lat = $request->lat;
        $verification->lng = $request->lng;
        //TODO: Move this to testable service
        $fileName = Str::random(64) . '.jpg';
        Storage::disk('local')->put($fileName, base64_decode($request->image));
        $verification->image_url = $fileName;
        $verification->save();
        return response()->json([
            'message' => 'Successfully submitted verification!'
        ], 201);
    }

}
