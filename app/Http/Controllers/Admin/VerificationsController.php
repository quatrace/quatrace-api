<?php

namespace App\Http\Controllers\Admin;

use App\Constants\VerificationStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewAdminVerificationRequest;
use App\Http\Requests\UserVerificationsRequest;
use App\User;
use App\Verification;
use Carbon\Carbon;
use Illuminate\Support\Str;

class VerificationsController extends Controller
{
    public function all(UserVerificationsRequest $request)
    {
        $verifications = Verification::where('user_id', $request->user_id)->orderBy('expires_at', 'DESC')->get();
        return response()->json(array_values($verifications->toArray()));
    }

    public function request(NewAdminVerificationRequest $request)
    {
        //TODO: Not testable code bellow
        $user = User::find($request->user_id);
        $user->verifications()->create([
            'token' => Str::random(config('quatrace.verification_token_length')),
            'expires_at' => Carbon::now()->addMinutes($user->settings->verification_ttl_minutes),
            'status' => VerificationStatus::NOT_SEND
        ]);
        return response()->json([
            'message' => 'Successfully submitted verification!',
        ], 201);
    }
}
