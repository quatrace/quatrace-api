<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewUserRequest;
use App\Notifications\NewTokenNotification;
use App\Services\TokenService;
use App\User;
use Carbon\Carbon;

class UsersController extends Controller
{

    protected TokenService $tokenService;

    public function __construct(TokenService $tokenService)
    {
        $this->tokenService = $tokenService;
    }

    public function all()
    {
        $users = User::has('quarantine')
            ->with('quarantine', 'verifications')
            ->get();
        return response()->json(array_values($users->toArray()));
    }

    /**
     * @param \App\Http\Requests\NewUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(NewUserRequest $request)
    {
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
        ]);
        $user->save();
        $user->quarantine()->create([
            'phone' => $request->phone,
            'ucn' => $request->ucn,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'expires_at' => Carbon::parse($request->quarantine_date),
        ]);
        $user->settings()->create();

        $payload = $this->tokenService->issue($user);
        $user->notify(new NewTokenNotification($payload));

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }
}
