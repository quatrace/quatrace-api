<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Setting;

class SettingsController extends Controller
{
    public function all()
    {
        $settings = Setting::first();
        return response()->json($settings);
    }
}
