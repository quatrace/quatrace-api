<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\NewAccessTokenRequest;
use App\Quarantine;
use App\Verification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class AuthController extends Controller
{
    /**
     * @param \App\Http\Requests\LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token', ['admin-access']);
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => __('Successfully logged out')
        ]);
    }

    /**
     * @param \App\Http\Requests\NewAccessTokenRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function token(NewAccessTokenRequest $request)
    {
        $quarantine = Quarantine::where('fcm_key', $request->fcm_key)->first();
        if (!$quarantine || !$quarantine->user) {
            return response()->json([
                'message' => 'FCM key not found'
            ], 401);
        }
        $tokenResult = $quarantine->user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        $user = $request->user();
        $data = [
            'name' => $user->name,
            'email' => $user->email,
            'created_at' => $user->created_at,
        ];
        $verifications = Verification::where('user_id', $user->id)->showOnMobile()->orderBy('created_at', 'DESC')->get();
        if ($user->quarantine) {
            $data = array_merge($data, [
                'phone' => $user->quarantine->phone,
                'lat' => $user->quarantine->lat,
                'lng' => $user->quarantine->lng,
                'quarantine_date' => $user->quarantine->expires_at,
                'fcm_key' => $user->quarantine->fcm_key,
                'verifications' => array_values($verifications->toArray())
            ]);
        }
        return response()->json($data);
    }
}
