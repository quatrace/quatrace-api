<?php

namespace App;

use App\Traits\DateSerializable;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use DateSerializable;

    protected $hidden = [
        'user_id',
        'is_superuser',
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'user_id',
        'is_superuser',
        'verification_frequency_minutes',
        'verification_ttl_minutes',
        'allowed_distance',
        'min_confidence',
    ];

    protected $casts = [
        'is_superuser' => 'bool',
        'verification_frequency_minutes' => 'int',
        'verification_ttl_minutes' => 'int',
        'allowed_distance' => 'int',
        'min_confidence' => 'float'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
