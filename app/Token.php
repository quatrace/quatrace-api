<?php

namespace App;

use App\Traits\DateSerializable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    use DateSerializable;

    protected $fillable = [
        'user_id',
        'payload',
        'expires_at',
    ];

    protected $dates = [
        'expires_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeValid($query)
    {
        $query->where('expires_at', '>', Carbon::now());
    }
}
