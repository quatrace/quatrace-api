<?php

namespace App\Providers;

use App\Contracts\FaceComparable;
use App\Services\ImaggaFaceAPIService;
use App\Services\VerificationService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(VerificationService::class, VerificationService::class);
        $this->app->bind(FaceComparable::class, ImaggaFaceAPIService::class);
    }
}
