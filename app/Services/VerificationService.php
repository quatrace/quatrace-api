<?php

namespace App\Services;

use App\Constants\VerificationStatus;
use App\Contracts\FaceComparable;
use App\Verification;
use Illuminate\Support\Facades\Storage;

class VerificationService
{
    protected FaceComparable $faceRecognitionService;

    public function __construct(FaceComparable $faceRecognitionService)
    {
        $this->faceRecognitionService = $faceRecognitionService;
    }

    /**
     * @param \App\Verification $verification
     * @return bool
     */
    public function validate(Verification $verification): bool
    {
        return $this->validateLocation($verification) && $this->validateFace($verification);
    }

    /**
     * @param \App\Verification $verification
     * @return bool
     */
    public function validateLocation(Verification $verification)
    {
        $user = $verification->user;
        $quarantine = $user->quarantine;

        $verification->status = VerificationStatus::REJECTED;
        $distance = coordinatesDistance($quarantine->lat, $quarantine->lng, $verification->lat, $verification->lng);
        $verification->distance = $distance;
        if ($distance > 500) {
            $verification->reason = 'You are more than 500 meters away from quarantine location';
            $verification->is_identical = false;
            $verification->confidence = 0;
            $verification->save();
            return false;
        }
        return true;
    }

    /**
     * @param \App\Verification $verification
     * @return bool
     */
    public function validateFace(Verification $verification)
    {
        $user = $verification->user;
        $quarantine = $user->quarantine;

        if (!Storage::disk('local')->exists($quarantine->image_url) || !Storage::disk('local')->exists($verification->image_url)) {
            $verification->reason = 'Problem with verification image';
            $verification->is_identical = false;
            $verification->confidence = 0;
            $verification->save();
            return false;
        }
        $imageComparisonResult = $this->faceRecognitionService->compare(
            Storage::disk('local')->get($quarantine->image_url),
            Storage::disk('local')->get($verification->image_url)
        );
        if (!$imageComparisonResult->isIdentical()) {
            $verification->reason = $imageComparisonResult->getReason() ? $imageComparisonResult->getReason() : 'We are not sure its you on the picture';
            $verification->is_identical = $imageComparisonResult->isIdentical();
            $verification->confidence = $imageComparisonResult->getConfidence();
            $verification->save();
            return false;
        }
        $verification->status = VerificationStatus::APPROVED;
        $verification->is_identical = $imageComparisonResult->isIdentical();
        $verification->confidence = $imageComparisonResult->getConfidence();
        $verification->save();
        return true;
    }

}
