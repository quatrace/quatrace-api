<?php

namespace App\Services;

use App\Contracts\FaceComparable;
use App\Types\FaceComparisonResult;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class MicrosoftFaceAPIService implements FaceComparable
{
    public function compare(string $original, string $candidate): FaceComparisonResult
    {
        Log::debug('original and candidate', ['original' => $original, 'candidate' => $candidate]);
        $result = new FaceComparisonResult();
        try {
            $userFaceId = $this->getFaceId($original);
            $matchFaceId = $this->getFaceId($candidate);
            if (!$userFaceId || !$matchFaceId) {
                return $result->setReason('No face detected');
            }
            $response = app(Client::class)
                ->post('https://' . config('quatrace.azure.endpoint') . '/face/v1.0/verify', [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Ocp-Apim-Subscription-Key' => config('quatrace.azure.api_key'),
                    ],
                    'body' => json_encode([
                        'faceId1' => $userFaceId,
                        'faceId2' => $matchFaceId
                    ])
                ]);
            $responseData = json_decode($response->getBody()->getContents());
            return $result->setIsIdentical($responseData->isIdentical)
                ->setConfidence($responseData->confidence * 100);
        } catch (\Exception $e) {
            Log::error($e);
            return $result
                ->setReason('Problem with external face recognition service')
                ->setIsIdentical(false)
                ->setConfidence(0.00);
        }
    }

    /**
     * @param $image
     * @return mixed
     */
    protected function getFaceId($image)
    {
        $response = app(Client::class)
            ->post('https://' . config('quatrace.azure.endpoint') . '/face/v1.0/detect?returnFaceId=true', [
                'headers' => [
                    'Content-Type' => 'application/octet-stream',
                    'Ocp-Apim-Subscription-Key' => config('quatrace.azure.api_key'),
                ],
                'body' => $image
            ]);
        return collect(json_decode($response->getBody()->getContents(), true))
            ->pluck('faceId')
            ->first();
    }
}
