<?php

namespace App\Services;

use App\Notifications\NewTokenNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;

class TokenService
{
    public function issue(User $user, string $payload = null)
    {
        $token = $user->loginTokens()->valid()->first();
        if ($token) {
            return $token->payload;
        }
        $payload = $payload ? $payload : Str::random(config('quatrace.login_tokens_length'));
        $user->loginTokens()->create([
            'payload' => $payload,
            'expires_at' => Carbon::now()->addHours(config('quatrace.login_tokens_validity_hours'))
        ]);

        return $payload;
    }
}
