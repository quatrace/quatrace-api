<?php

namespace App\Services;

use App\Contracts\FaceComparable;
use App\Types\FaceComparisonResult;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class ImaggaFaceAPIService implements FaceComparable
{
    public function compare(string $original, string $candidate): FaceComparisonResult
    {
        $result = new FaceComparisonResult();
        try {
            $userFaceId = $this->getFaceId($original);
            $matchFaceId = $this->getFaceId($candidate);
            if (!$userFaceId || !$matchFaceId) {
                return $result->setReason('No face detected');
            }
            $response = app(Client::class)
                ->request('GET', config('quatrace.imagga.endpoint') . '/v2/faces/similarity', [
                    'headers' => [
                        'Authorization' => 'Basic ' . config('quatrace.imagga.api_key'),
                    ],
                    'query' => [
                        'face_id' => $userFaceId,
                        'second_face_id' => $matchFaceId
                    ]
                ]);
            $responseData = json_decode($response->getBody()->getContents());
            $isIdentical = $responseData->result && $responseData->status->type === 'success' && (float)$responseData->result->score > 70;
            return $result
                ->setReason($responseData->status->text)
                ->setIsIdentical($isIdentical)
                ->setConfidence((float)$responseData->result->score);
        } catch (\Exception $e) {
            Log::error($e);
            return $result
                ->setReason('Problem with external face recognition service')
                ->setIsIdentical(false)
                ->setConfidence(0.00);
        }
    }

    /**
     * @param $image
     * @return mixed
     */
    protected function getFaceId($image)
    {
        $response = app(Client::class)
            ->post(config('quatrace.imagga.endpoint') . '/v2/faces/detections', [
                'headers' => [
                    'Authorization' => 'Basic ' . config('quatrace.imagga.api_key'),
                ],
                'form_params' => [
                    'image' => $image,
                    'return_face_id' => 1
                ]
            ]);
        $response = json_decode($response->getBody()->getContents(), true);
        if (!array_key_exists('result', $response)) {
            Log::error('Bad response from Imagga', [$response]);
            return null;
        }
        if (!array_key_exists('faces', $response['result'])) {
            Log::error('No faces detected', [$response]);
            return null;
        }
        $faces = collect($response['result']['faces']);
        if (count($faces) > 1) {
            Log::error('More than one face on picture', [$response]);
            return null;
        }

        return $faces->pluck('face_id')->first();
    }
}
