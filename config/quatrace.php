<?php

return [
    'login_tokens_validity_hours' => env('LOGIN_TOKENS_VALIDITY_HOURS', 6),
    'login_tokens_length' => env('LOGIN_TOKENS_LENGTH', 12),
    'verification_token_length' => env('VERIFICATION_TOKEN_LENGTH', 64),
    'admin' => [
        'emails' => explode(",", env('ADMIN_EMAILS', 'contact@scratcher.io')),
    ],
    'azure' => [
        'api_key' => env('AZURE_API_KEY', ''),
        'endpoint' => env('AZURE_ENDPOINT', ''),
    ],
    'imagga' => [
        'api_key' => env('IMAGGA_API_KEY', ''),
        'endpoint' => env('IMAGGA_ENDPOINT', ''),
    ]
];
